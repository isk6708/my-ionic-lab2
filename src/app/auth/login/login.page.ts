import { Component, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

  @Output() userLogged= new EventEmitter();

  email="";
  password="";
  alertMessage="";
  isAlertOpen = false;
  public alertButtons = ['OK'];

  
  constructor() { }

  ngOnInit() {
  }

  setOpen(isOpen: boolean) {
    this.isAlertOpen = isOpen;
  }

  returnToLogin(){
    this.isAlertOpen = false;
    window.location.href="/login";
  }
  async logMeIn(){
    const data = await fetch('http://localhost:3002/auth/jwt',
    {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(
        { 
        email: this.email,
        password: this.password})
      })

      const content = await data.json();
      if (content.message === 'ok'){
        const token = content.token;
        console.log(content.token);        
         
        if (content.message === 'ok'){
          window.localStorage.setItem('htp2auth',token);
          this.userLogged.emit();
          window.location.href="/staff";
        }
      }else{
        console.log(content);
        this.isAlertOpen = true;
        this.alertMessage = content.message;
        // 
      }
      
  }

}
