import { Component,Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})

export class AppComponent {
  @Output() userLogged = new EventEmitter();
  @Input() showLogin=false;

  activity='';

  constructor() {}
  ngOnInit(){
    this.userLogged.emit();
  }

  onLogin(){
    this.showLogin = true;
  }

  onLogOut(){
    this.showLogin = false;
  }

  logOut(){
    window.localStorage.removeItem('htp2auth');
    window.location.href = "/login";
    this.userLogged.emit();
    this.showLogin = true;
  }

  logIn(){
    // alert('test');
    this.showLogin = false;
  }
}
