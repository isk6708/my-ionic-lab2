import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {
  products=[];
  prodDetail:any=[];  
  showDetail=false;
  showList=true;
  constructor(private alertController: AlertController) {}
  ngOnInit() {
    this.getData();
  }
  async viewDetail(id:number){
    const response = await fetch("https://fakestoreapi.com/products/"+id);
    this.prodDetail = await response.json();
    this.showDetail=true;
    this.showList=false;
    // const alert1 = await this.alertController.create({
    //   header: 'Alert',
    //   subHeader: this.prodDetail['title'],
    //   message: 'Price:'+this.prodDetail['price'],
    //   buttons: ['OK'],
    // });

    // await alert1.present();
  }

  async getData(){
    const response = await fetch("https://fakestoreapi.com/products");
    this.products = await response.json();
    console.log(this.products);
  }

}
