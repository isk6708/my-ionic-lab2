import { AlertController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-staff',
  templateUrl: './staff.page.html',
  styleUrls: ['./staff.page.scss'],
})
export class StaffPage implements OnInit {
  proxy="http://localhost:3002";
  person=[];
  user_id:number=0;
  username="";
  email="";
  password="";
  search_username="";
  showAddUser=false;
  showList=true;
  newUser=true;

  alertMessage="";
  alertSubMessage=""
  isAlertOpen = false;
  public alertButtons = ['OK'];

  constructor(private alertController:AlertController) { }

  ngOnInit() {
    this.getData();
  }

  reconnect(){
    this.person=[];
    this.getData();
  }
  async deleteUser(id:number){
    if (confirm('And pasti?')){
      const data = await fetch(this.proxy +'/pengguna/hapus-pengguna/'+id,
      {
        method: 'DELETE',
      });
      const content = await data.json();
      console.log(content);
      this.getData();
    }
  }

  async addUser(){
    const data = await fetch(this.proxy +'/pengguna/create-user',
    {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(
        {username: this.username, 
        email: this.email,
        password: this.password})
      })

      const content = await data.json();

      console.log(content);
      this.showAddUser=false;
      this.getData();
  }

  async retrieveStaff(id:number){
    const response = await fetch(this.proxy +"/pengguna/"+id);
    const data = await response.json();
    this.username = data['username'];
    this.email = data['email'];
    this.password = data['password'];
    this.user_id = id;
    this.showList = false;
    this.showAddUser = true;
    this.newUser = false;
  }

  async updateStaff(){
    if (confirm('Anda Pasti?')){
      this.showList = true;
      this.showAddUser = false;
      this.newUser = true;
      let data:any='';
      if (this.password.length <= 12){
        data = await fetch(this.proxy +'/pengguna/kemaskini-pengguna/'+this.user_id,
        {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        
        body: JSON.stringify(
          {username: this.username, 
          email: this.email,
          password: this.password.length <=12?this.password:''
        })
        })
      }else{
         data = await fetch(this.proxy +'/pengguna/kemaskini-pengguna/'+this.user_id,
          {
          method: 'PUT',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          
          body: JSON.stringify(
            {
              username: this.username, 
              email: this.email
          })
          })
      }
      

      const content = await data.json();

      console.log(content);

      this.getData();
    }
    
  }
  returnToLogin(){
    this.isAlertOpen = false;
    if (this.alertSubMessage !== "Server Problem")
      window.location.href="/login";
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: this.alertSubMessage,
      message: this.alertMessage,
      buttons: this.alertButtons,
    });
  
    await alert.present();
    let result = await alert.onDidDismiss().then(data=>{this.isAlertOpen=false;this.returnToLogin()});
    // console.log(result);
  }

  async getData(reset=false){
    if (reset)
      this.search_username="";
    
    this.showAddUser = false;
    this.showList = true;

    try{
      // console.log('Test '+ window.localStorage.getItem('htp2auth'));
      return await fetch(this.proxy +"/pengguna/senarai-pengguna-jwt?username="+this.search_username,
      {
        // method:'GET',
        headers: {
          'Authorization': 'jwt '+ window.localStorage.getItem('htp2auth')
        },

      })
      .then(res=>res.json())
      .then(res => {
          this.person = res;          
      })
  }catch (error) {
    if (error instanceof Error) {
      console.log('Test error message ONE: ', error.message);
      // this.alertMessage="Sila log masuk terlebih dahulu1";
      // this.isAlertOpen = true;  
      
      if (error.message === 'Failed to fetch'){
        this.alertSubMessage = "Server Problem";
        this.alertMessage="Sila cuba lagi";
        
      }else{
        this.alertSubMessage = "Authorization Problem";
        this.alertMessage="Sila log masuk terlebih dahulu";
        
      }
        
      // alert('error message: '+ error.message);
      // console.log('Error '+ window.localStorage.getItem('htp2auth'));
      // return error.message;
    } else {
      
      console.log(' Test unexpected error TWO: ', error);
      // alert('unexpected error:'+ error);
      // return 'An unexpected error occurred';
      // this.alertMessage="Sila log masuk terlebih dahulu2";
      // this.isAlertOpen = true;
      
    }
    this.presentAlert();
  }
  }
    // const response = await fetch(this.proxy +"/pengguna/senarai-pengguna?username="+this.search_username);
    // this.person = await response.json();
    // console.log(this.person);
  // }

}
