import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.page.html',
  styleUrls: ['./attendance.page.scss'],
})

export class AttendancePage implements OnInit {

  constructor() { }
  
  user = "Raj";
  attendances = ['2023-06-01 09:21', '2023-06-02 08:51'];
  curr_time = new Date();
  // m :any = this.curr_time.getDate();
  // d :any = this.curr_time.getDay();
  // y :any = this.curr_time.getFullYear();
  // h :any = this.curr_time.getHours;
  // i :any = this.curr_time.getMinutes;

  ngOnInit() {
  }

  captureAttendance(){

    if (confirm('Anda Pasti?')){
      let m :any = this.curr_time.getDate();
      let d :any = this.curr_time.getDay();
      let y :any = this.curr_time.getFullYear();
      let h :any = this.curr_time.getHours();
      let i :any = this.curr_time.getMinutes();
      
      this.attendances.push(y+'-'+m+'-'+d+ ' ' + h+':'+i);
    }
  }

}
